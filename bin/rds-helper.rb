#
# This program retrieves certain bits of information about RDS instances.
#
# This program required the following gems:
#   thor
#   aws-sdk

require "thor"
require "aws-sdk"


class RdsHelper < Thor
  
  # latestsnapshot
  #
  # Retrieve all the current snapshots for a given RDS DB by its identifier.
  # Look through all the snapshots and find the most recent.
  # Echo the snapshot identifier to STDOUT
  #
  desc "latestsnapshot DBNAME", "Retrieve the latest snapshot name for DBNAME."
  def latestsnapshot(dbname=nil)
    rds = Aws::RDS::Client.new(region: 'us-west-2')
    resp = rds.describe_db_snapshots({
      db_instance_identifier: dbname,
    })
    
    newestSnapshot = nil
    newestTime = nil
    resp.db_snapshots.each do |s|
      # 2016-01-28 10:16:17 UTC
      if newestTime == nil or s.snapshot_create_time > newestTime
        newestTime = s.snapshot_create_time
        newestSnapshot = s
      end
    end
    
    puts newestSnapshot.db_snapshot_identifier
    
  end
  
end
 
RdsHelper.start(ARGV)

