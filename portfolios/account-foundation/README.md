# Account Foundation

This project defines several CloudFormation templates and Lambda functions to be
run at the creation of new AWS accounts.

## Lambda Functions:
* Tag EC2 Instances with 'createdby'
* Tag OpsWorks Instances: Since OpsWorks instances don't tag their underlying EC2 instances, this function listens for EC2 create events and tags the instance according to data in the OpsWorks custom JSON field.

## Setup

### Ansible Deployer

This project is deployed via Ansible, and so needs to have a host with ansible installed on it, 
and also with permissions to do all the account setup functions.  There is an accompanying project
called 'ansible-deployer' which can be used to run this project from.  See that project for 
setup details.

### Lambda Source Staging
The source code for the Lambda functions must be placed into an existing S3 bucket which is 
publicly accessible. The path and bucketname must be passed in to the main new account playbook
script when its run.

Initially, and when there are code updates to the lambda functions, you must run the 'staging' 
playbook which will package up the lambda functions and put them into the S3 distribution bucket.

    $ ansible-playbook stage-files.yaml

You will be asked for an AWS Access / Secret Key pair which will need to have PutObject permissions
on the S3 bucket/path to hold the contents of the 'src' directory.

## New Account Deployment

When setting up a new AWS account, run the following:

    $ ansible-playbook -v new-account-playbook.yaml

This will kick off the ansible playbook and deploy all the resources required for a new AWS account.



## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
