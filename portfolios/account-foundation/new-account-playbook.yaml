---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    stack_prefix: "fdn"
    region: "us-west-2"
    owner: "fischerm"
    centralLoggingBucketName: "uaz-cloudtrail-bucket"
    functionS3Bucket: "ua-uits-ecs-public"
    functionS3Path: "lambda"
    samlProviderExists: false

  vars_prompt:
    - name: "alarmEmail"
      prompt: "Email Address for Alarm Notification?"
      private: no
    - name: "accountType"
      prompt: "Production or Non-Production account?"
      default: "Non-Production"
      private: no
    - name: "serviceTag"
      prompt: "ServiceTag?"
      private: no
    - name: "environmentTag"
      prompt: "EnvironmentTag?"
      private: no
    - name: "contactNetidTag"
      prompt: "ContactNetidTag?"
      private: no
    - name: "accountNumberTag"
      prompt: "AccountNumberTag?"
      private: no
    - name: "ticketNumberTag"
      prompt: "TicketNumberTag?"
      private: no


  tasks:

  # Use the AWS EC2 Metadata service to retrieve basic information about this EC2
  # instance. This will be used to learn the AWS Account ID, Region, etc.  
  - name: Get Account Info for EC2 Instance
    shell: curl http://169.254.169.254/latest/dynamic/instance-identity/document
    register: instance_info
  
  # Store the results of the above CURL output on an easy to use variables
  # This JSON data structure looks something like this:
  #
  # {
  #   "devpayProductCodes" : null,
  #   "privateIp" : "10.221.72.213",
  #   "accountId" : "998687558142",
  #   "availabilityZone" : "us-west-2a",
  #   "version" : "2010-08-31",
  #   "instanceId" : "i-e5860ffd",
  #   "billingProducts" : null,
  #   "instanceType" : "t2.micro",
  #   "pendingTime" : "2016-10-04T16:46:06Z",
  #   "architecture" : "x86_64",
  #   "imageId" : "ami-7172b611",
  #   "kernelId" : null,
  #   "ramdiskId" : null,
  #   "region" : "us-west-2"
  # }
  #
  - name: Store basic info in variables
    set_fact:
      accountId:        "{{ (instance_info.stdout | from_json).accountId }}"
      region:           "{{ (instance_info.stdout | from_json).region }}"
      availabilityZone: "{{ (instance_info.stdout | from_json).availabilityZone }}"

  # See if this account already has an Identity provider set
  - name: List Existing SAML Identity Providers
    shell: aws iam list-saml-providers
    register: samlListResult
  
  - name: Store existing SAML Provider state
    when:
      - "{{ ( (samlListResult.stdout | from_json).SAMLProviderList | length ) }} > 0"
      - "'UA_Shibboleth_IdP' in '{{ (samlListResult.stdout | from_json).SAMLProviderList[0].Arn }}'"
    set_fact:
      samlProviderExists: true

  # Store the ARN of the existing SAML provider.
  - name: Store SAML IdP ARN
    when: samlProviderExists == true
    set_fact:
      samlProviderArn: "{{ (samlListResult.stdout | from_json).SAMLProviderList[0].Arn }}"

  # Download the UA SAML Metadata XML document so we always use an up-to-date version.
  - name: download ua-incommon-metadata.xml
    when: samlProviderExists != true
    get_url:
      url: https://shibboleth.arizona.edu/metadata/ua-incommon-metadata.xml
      dest: "{{ playbook_dir }}/ua-incommon-metadata.xml"

  # Make an aws-cli call to create the SAML Identity Provider since Ansible doesn't
  # support this natively.
  - name: Create AWS SAML Identity Provider
    when: samlProviderExists != true
    shell: aws iam create-saml-provider --saml-metadata-document "file://{{ playbook_dir }}/ua-incommon-metadata.xml" --name UA_Shibboleth_IdP
    register: samlCreateResult

  # Store the ARN returned from the aws-cli call.
  - name: Store SAML IdP ARN
    when: samlProviderExists != true
    set_fact:
      samlProviderArn: "{{ (samlCreateResult.stdout | from_json).SAMLProviderArn }}"

  # First off deploy the basic IAM resources
  - name: Deploy Foundation IAM User Roles
    action: cloudformation
      stack_name={{ stack_prefix }}-iam
      state=present
      region="{{region}}"
      template=templates/foundation-user-roles.yaml
    args:
      template_parameters:
        ShibbolethIDPARN: "{{ samlProviderArn }}"

  # Next deploy the Logging template
  - name: Deploy Foundation Logging
    action: cloudformation
      stack_name={{ stack_prefix }}-logging
      state=present
      region="{{region}}"
      template=templates/foundation-logging.yaml
    args:
      template_parameters:
        pNotifyEmail:           "{{ alarmEmail }}"
        pSupportsGlacier:       true
        pCloudTrailLogBucket:   "{{ centralLoggingBucketName }}"
        pAccountType:           "{{ accountType }}"
        ServiceTag:             "{{ serviceTag }}"
        EnvironmentTag:         "{{ environmentTag }}"
        ContactNetidTag:        "{{ contactNetidTag }}"
        AccountNumberTag:       "{{ accountNumberTag }}"
        TicketNumberTag:        "{{ ticketNumberTag }}"
    register: loggingStack

  # Create any Lambda specific IAM roles
  - name: Deploy Lambda IAM Roles
    action: cloudformation
      stack_name={{ stack_prefix }}-lmd-roles
      state=present
      region="{{region}}"
      template=templates/foundation-lambda-roles.yaml
    register: lambdaIAMStack

  # Create a sleep-delay lambda function for use as a custom CloudFormation resource.
  # Requires the Logging Lambda role from previous step
  - name: Deploy Lambda SleepDelay Function
    action: cloudformation
      stack_name={{ stack_prefix }}-lmd-sleep
      state=present
      region="{{region}}"
      template=templates/lambda-sleep-delay.yaml
    args:
      template_parameters:
        SleepDelayRoleARN: "{{ lambdaIAMStack.stack_outputs.LambdaLogRoleARN }}"
    register: sleepFnStack

  # We renamed one of the CloudFormation stacks, so see if the old one still exists so 
  # we can delete it.
  - name: Check for CloudFormation Stack
    shell: aws cloudformation --region "{{region}}" describe-stacks --stack-name "{{ stack_prefix }}-lmd-fn"
    ignore_errors: yes
    register: oldLmdFnStackResult

  # Delete old lmd-fn stack as we renamed it to lmd-opsworks-tags.
  - name: Delete old fdn-lmd-fn stack
    when: oldLmdFnStackResult.stdout != ''
    action: cloudformation
      stack_name={{ stack_prefix }}-lmd-fn
      state=absent
      region="{{region}}"
      template=templates/lambda-opsworks-tags.yaml

  # Deploy OpsWorks Tagging Lambda Function. Requires sleep-delay ARN from previous step.
  - name: Deploy OpsWorks Tagging Lambda Functions
    action: cloudformation
      stack_name={{ stack_prefix }}-lmd-opsworks-tags
      state=present
      region="{{region}}"
      template=templates/lambda-opsworks-tags.yaml
    args:
      template_parameters:
        FunctionS3Bucket:       "{{ functionS3Bucket }}"
        FunctionS3Path:         "{{ functionS3Path }}"

  # Deploy EC2 Tagging Lambda Function. Requires sleep-delay ARN from previous step.
  - name: Deploy EC2 Tagging Lambda Functions
    action: cloudformation
      stack_name={{ stack_prefix }}-lmd-createdby
      state=present
      region="{{region}}"
      template=templates/lambda-createdby.yaml
    args:
      template_parameters:
        FunctionS3Bucket:       "{{ functionS3Bucket }}"
        FunctionS3Path:         "{{ functionS3Path }}"

