from __future__ import print_function
import json
import boto3
import re

"""
This lambda function should be triggered by a CloudWatch Event Rule:

{
  "detail-type": [
    "AWS API Call via CloudTrail"
  ],
  "detail": {
    "eventSource": [
      "ec2.amazonaws.com"
    ],
    "eventName": [
      "RunInstances"
    ]
  }
}

This lambda function will tag EC2 instances with the ARN of whatever
initiated the creation of the resource.

"""

ec2=boto3.resource("ec2")
rds=boto3.client("rds")

def GetTags(context):
  Tags=[]
  if 'arn' in context:
    # Strip off the first bits of an ARN
    # arn:aws:iam::998687558142:user/fischerm  ->  user/fischerm
    p = re.compile('arn:aws:[a-z]{3}:[a-zA-Z0-9]*:[0-9]*:')
    createdby = p.sub("", context['arn'])
    Tags.append({"Key": "createdby", "Value": createdby})

  return Tags

def RunInstancesHandler(response, context):
  try:
      #Get owner from response dict
      principal=context["principalId"]
      # loop through instances in response and record their ids,
      # do the same for their volumes
      ec2Resources=[]
      for instance in response["instancesSet"]["items"]:
       ec2Resources.append(instance["instanceId"])
      instances=ec2.instances.filter(InstanceIds=ec2Resources)
      for instance in instances:
        for vol in instance.volumes.all():
          ec2Resources.append(vol.id)
      ec2.create_tags(Resources=ec2Resources, Tags=GetTags(context))
  except Exception as err:
    print ("Error Tagging EC2 resource(s): ", err)
    raise

def CreateImageHandler(response, context):
  ec2.create_tags(Resources=[response["imageId"]], Tags=GetTags(context))

def CreateVolumeHandler(response, context):
  ec2.create_tags(Resources=[response["volumeId"]], Tags=GetTags(context))

def CreateSnapshotHandler(response, context):
  ec2.create_tags(Resources=[response["snapshotId"]], Tags=GetTags(context))

def CreateDBInstanceHandler(response, context):
  rds.add_tags_to_resource(ResourceName=response["dBInstanceArn"], Tags=GetTags(context))

def CreateDBSnapshotHandler(response, context):
  rds.add_tags_to_resource(ResourceName=response["dBSnapshotArn"], Tags=GetTags(context))

handleAPICall={
  "RunInstances": RunInstancesHandler,
  "CreateImage": CreateImageHandler,
  "CreateVolume": CreateVolumeHandler,
  "CreateSnapshot": CreateSnapshotHandler,
  "CreateDBInstance": CreateDBInstanceHandler,
  "CreateDBSnapshot": CreateDBSnapshotHandler
}

def CloudwatchEventTagging_handler(event, context):
    try:
      responseElements=""
      usercontext=""
      if "detail" in event:
        eventDetail=event["detail"]
        if "eventName" in eventDetail:
          eventName=eventDetail["eventName"]
          if "responseElements" in eventDetail:
            responseElements=eventDetail["responseElements"]
          if "userIdentity" in eventDetail:
            userIdentity=eventDetail["userIdentity"]
          if eventName in handleAPICall:
            handleAPICall[eventName](responseElements,userIdentity);
    except Exception as err:
        print("Error in Tagging Function: ", err)
