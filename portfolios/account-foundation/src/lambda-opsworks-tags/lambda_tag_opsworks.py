"""
UITS OpsWorks Instance Tagging

This script tags the EC2 instances created by OpsWorks based on
information in the 'custom JSON' section of the OpsWorks stack. 

That custom JSON should take the following form:

{
  "opsworks_tags": {
    "instances": {
      "owner": "Mark Fischer",
      "netid": "fischerm",
      "projectname": "ECS Tag Testing"
    }
  }
}

This function should be invoked by a CloudWatch Event Rule.
  Event Source: ec2.amazonaws.com
  Event Name: RunInstances

Then this function will be called each time an EC2 instance is created.
If that instance is being spun up as part of an OpsWorks stack, and
the requisite custom JSON is present on that stack, then the tags are
applied to this EC2 instance.

"""
from __future__ import print_function
import json
import boto3
import logging
import time
import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

aws_session = boto3.Session()

def lambda_handler(event, context):
    # print('Received event: ' + json.dumps(event, indent=2))
    
    ec2 = aws_session.client('ec2', region_name='us-west-2')
    opw = aws_session.client('opsworks', region_name='us-east-1')
    
    # See what AWS service initiated this function call.
    if "invokedBy" not in event['detail']['userIdentity']:
      logger.info("Not invoked by another service. Exiting")
      return "Non-OpsWorks Invocation"
    
    invoker = event['detail']['userIdentity']['invokedBy']['serviceName']
    logger.info("OpsWorks Instance Tagger invoked by: " + invoker)
    if invoker != "opsworks.amazonaws.com":
      logger.info("Not invoked by OpsWorks. Exiting")
      return "Non-OpsWorks Invocation"

    # Get the OpsWorks Instance ID from the event data
    opsworks_instance_id = event['detail']['requestParameters']['clientToken']
    
    # Lookup this OpsWorks instance and get the associated Stack and EC2 IDs
    instances_result = opw.describe_instances( InstanceIds=[opsworks_instance_id] )
    instance = instances_result[u'Instances'][0]
    opsworks_stack_id = instance[u'StackId']
    ec2_instance_id = instance[u'Ec2InstanceId']
    
    # Look up the stack and get the custom JSON
    stacks_result = opw.describe_stacks( StackIds=[ opsworks_stack_id ])
    stack = stacks_result['Stacks'][0]
    
    if "CustomJson" not in stack:
      logger.info("No OpsWorks custom JSON. Exiting")
      return "No JSON Data"
    
    custom_json_str = stack['CustomJson']
    custom_json = json.loads(custom_json_str)
    
    # Check to see if we have tagging data in the json
    if "opsworks_tags" not in custom_json or "instances" not in custom_json['opsworks_tags']:
      logger.info("No Tag Data found in OpsWorks custom JSON. Exiting")
      return "No JSON Tag Data"

    tags_to_set = []
    for k,v in custom_json['opsworks_tags']['instances'].iteritems():
      tags_to_set.append( {u'Key': k, u'Value': v} )
    
    # Tag this instance
    ec2.create_tags(Resources=[ec2_instance_id], Tags=tags_to_set)
    
    logger.info("Tagged instance: " + ec2_instance_id)
    logger.info(tags_to_set)
    
    