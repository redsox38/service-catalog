# EFS File System Volume

This template creates an AWS EFS (Elastic File System) volume. This is Amazon's
managed NFS service.


## Goals
* Create an EFS volume and all associated support resources


## Usage

This templates creates the following resources:

### EFS Volume
An AWS EFS volume. Store your files here!

### Security Group for the EFS Volume
This security group wraps the EFS volume, and defines what can talk to it. It allows
traffic in only from resources in the corresponding Target Security Group. See next.

### Target Security Group
This security group is not applied to anything directly in this template. It also defines
no rules. It exists only as a logical container to be used later in order to allow
resources access to the EFS volume.  ie you would add this security group to an EC2
instance in order for that instance to mount this EFS volume.


## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
