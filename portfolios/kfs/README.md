# UAccess Financials (Kuali Financial System - KFS)

This project contains templates relating to the automated deployment of the KFS system.

Current Templates
* OpsWorks Development Stacks (dev, test, etc)


## Goals
* Provide a quick way to spin up a new KFS environment
* Allow support staff to easily deploy new environments as needed

## Foundation/Installation

To deploy the foundation for this blueprint:

Unknown currently


## Usage

Unknown currently


## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
