# Archive S3 Bucket

This blueprint creates an S3 Bucket and an IAM account that has full access to it. 
The S3 bucket has lifecycle policies attached to it which will move data down to
cheaper but less accessible storage over time, and eventually deleted.

The minimum values for each of the migrations are below. Any of these values can be
increased to meet the needs of your archive.

* Initial Upload to Normal S3 Bucket
* After at least 30 days, items will be migrated to S3 "Infrequent Access" Storage
* After at least another 30 days (60 days total) the item will be moved to Glacier
* After at least 90 days in Glacier the items will be deleted

## Parameters

* BucketName
* DaysToInfrequentAccess
* DaysToGlacier
* DaysToDeletion
* OwnerTag
* OwnerNetidTag
* ProjectNameTag

## Outputs

* BucketName
* AccessKey For New IAM User
* SecretKey For New IAM User


## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
