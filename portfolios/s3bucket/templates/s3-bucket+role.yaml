---
# S3 Bucket & Role CloudFormation Deployment
# -----------------------------------------
# 
# This template creates an S3 bucket and a role to go along with it.
# 
# * An S3 bucket
# * An IAM Policy which allows access to that S3 bucket
# 


AWSTemplateFormatVersion: '2010-09-09'

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  # The name of the bucket.
  BucketName:
    Type: String
    Description: The name of the S3 Bucket.

  # #### Tags
  #
  # The following tags are applied to all resources created by this template.
  OwnerTag:
    Type: String
    Description: The owner or sponsor of this bucket. All resources will be tagged
      with this as the owner tag.
  OwnerNetidTag:
    Type: String
    Description: The name of the owner or sponsor for this website. All resources
      will be tagged with this as the netid tag.
  ProjectNameTag:
    Type: String
    Description: The project name of the owner or sponsor for this website. All resources
      will be tagged with this as the projectname tag.

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: S3 Bucket Configuration
      Parameters:
      - BucketName
    - Label:
        default: Tagging and Cost Management
      Parameters:
      - OwnerTag
      - OwnerNetidTag
      - ProjectNameTag
    ParameterLabels:
      BucketName:
        default: 'Bucket Name:'
      OwnerTag:
        default: 'Owner''s Name:'
      OwnerNetidTag:
        default: 'NetID:'
      ProjectNameTag:
        default: 'Project Name:'

# Resources
# ---------
#
# These are all of the resources deployed by this template.
#
Resources:

  # #### S3 Bucket    
  #
  # This deploys the S3 bucket with some tags.
  S3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Ref "BucketName"
      AccessControl: Private
      Tags:
      - Key: projectname
        Value: !Ref "ProjectNameTag"
      - Key: owner
        Value: !Ref "OwnerTag"
      - Key: netid
        Value: !Ref "OwnerNetidTag"
      - Key: blueprint
        Value: s3bucket

  # #### S3 Bukcet Policy
  #
  # Creates an IAM policy that can only connect to the S3 bucket specified.
  S3BucketPolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: !Sub "s3access-${BucketName}"
      Roles: 
        - !Ref EnvInstanceRole
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Effect: Allow
          Action:
          - s3:List*
          Resource:
          - "*"
        - Effect: Allow
          Action:
          - s3:*
          Resource: !Sub "arn:aws:s3:::${S3Bucket}/*"

  # #### Instance Role
  #
  # This is the IAM role that will be applied to the EC2 Instance. Any AWS specific
  # permissions that the node might need should be defined here.
  EnvInstanceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: "/"

  # #### Instance Profile
  #
  # This is just a little construct to connect a role together into a profile. The profile
  # is referenced by the EC2 Instance.
  EnvInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: "/"
      Roles: 
      - !Ref EnvInstanceRole

# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  BucketName:
    Value: !Ref S3Bucket
  BucketRole:
    Value: !Ref EnvInstanceRole
  InstanceProfile:
    Value: !Ref EnvInstanceProfile


