# Toolshed

This cloudformation template defines several internal infrastructure applications

* OpsWeb
* account.arizona.edu



## Parameters

* 
## Outputs

* 

## Creating via CLI

    $ aws cloudformation create-stack \
     --region us-west-2 \
     --capabilities CAPABILITY_IAM \
     --stack-name "ToolshedDev"  \
     --template-body "file://$PWD/service-catalog/portfolios/toolshed/templates/toolshed.json" \
     --parameters "file://$PWD/toolshed-params.json"

## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
